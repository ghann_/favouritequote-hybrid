import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { SettingPage } from "../pages/setting/setting";
import { LibraryPage } from "../pages/library/library";
import { SignInPage } from "../pages/sign-in/sign-in";
import { SignUpPage } from "../pages/sign-up/sign-up";
import { AuthService } from "../services/AuthService";

import firebase from "firebase";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  isLoggedIn = false;
  tabsPage = TabsPage;
  settingPage = SettingPage;
  libraryPage = LibraryPage;
  signInPage = SignInPage;
  signUpPage = SignUpPage;
  
  @ViewChild("sideMenu") nav: NavController;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private menuCtrl: MenuController, private authService: AuthService) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });

    firebase.initializeApp({
      apiKey: "AIzaSyDh8-rR0BFdBN9HIXiF371nXNicjqJLISA",
      authDomain: "favoritequotesapp-hybrid.firebaseapp.com",
      databaseURL: "https://favoritequotesapp-hybrid.firebaseio.com/"
    }); 

    firebase.auth().onAuthStateChanged(user =>{
      if(user){
        this.authService.setCurrentUser(user.uid)
        this.isLoggedIn = true;
        this.nav.setRoot(this.tabsPage)
      }else{
        this.isLoggedIn = false;
        this.nav.setRoot(this.signInPage)
      }
    });
  }

  onLoad(page: any){
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  logout(){
    this.authService.logout()
  }

}
