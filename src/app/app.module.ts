// unrealBots. 2017.

import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { LibraryPage } from '../pages/library/library';
import { FavoritePage } from '../pages/favorite/favorite';
import { SettingPage } from '../pages/setting/setting';
import { TabsPage } from '../pages/tabs/tabs';
import { QuotesPage } from '../pages/quotes/quotes';
import { QuotePage } from '../pages/quote/quote';
import { AddQuotePage } from '../pages/add-quote/add-quote';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { QuotesService } from '../services/quotes';
import { SettingService } from "../services/settings";
import { SignInPage } from "../pages/sign-in/sign-in";
import { SignUpPage } from "../pages/sign-up/sign-up";
import { AuthService } from "../services/AuthService";
import { PopoverPage } from "../pages/popover/popover";

@NgModule({
  declarations: [
    MyApp,
    LibraryPage,
    FavoritePage,
    TabsPage,
    SettingPage,
    QuotesPage,
    QuotePage,
    AddQuotePage,
    SignInPage,
    SignUpPage,
    PopoverPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{
      tabsPlacement: 'bottom',
      pageTransition: 'ios-transition',
      backButtonIcon: 'ios-arrow-back',
      iconMode: 'md',
      platforms: {
        ios: {
          backButtonText: '',
        },
        android: {
          backButtonText: 'Go Back',
        },
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LibraryPage,
    FavoritePage,
    TabsPage,
    SettingPage,
    QuotesPage,
    QuotePage,
    AddQuotePage,
    SignInPage,
    SignUpPage,
    PopoverPage
  ],
  providers: [
    QuotesService,
    SettingService,
    AuthService,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
