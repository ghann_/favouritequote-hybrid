import { Quote } from '../data/quote.interface';
import { AuthService } from "../services/AuthService";
import { Http } from '@angular/http';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';

@Injectable()
export class QuotesService {
    private favoriteQuotes: Quote[] = [];

    constructor (private http: Http, private authService: AuthService) { }

    addQuoteToFavorites(quote: Quote){
        this.favoriteQuotes.push(quote);
    }

    removeQuoteFromFavorites(quote: Quote){
        var index = this.favoriteQuotes.indexOf(quote);
        this.favoriteQuotes.splice(index, 1);   
    }

    removeAllQuote(){
        this.favoriteQuotes = [];
    }

    getFavoriteQuotes() {
        return this.favoriteQuotes.slice();
    }

    isFavorite(quote: Quote){
        return this.favoriteQuotes.indexOf(quote) == -1 ? false : true
    }

    storeList(token: string, uid: string){
        return this.http
            .put('https://favoritequotesapp-hybrid.firebaseio.com/' + uid + '/fav-quotes.json?auth=' + token, this.favoriteQuotes)
            .map((response: Response) => {
                return response.json();
            });
    }

}