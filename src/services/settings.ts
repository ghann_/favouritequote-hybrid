export class SettingService {
    private backgroundChanged: Boolean = false;

    private tabsPlacement: string = "bottom";
    private transitionStyle: string = "ios";

    changeBackground(key: Boolean){
      this.backgroundChanged = key;
    }

    isBackgroundChanged(){
     return this.backgroundChanged;
    }

    getCurrentTabsPlacement(){
      return this.tabsPlacement;
    }
    setCurrentTabsPlacementTo(key: string){
      this.tabsPlacement = key;
    }

    getCurrentTransitionStyle(){
      return this.transitionStyle;
    }
    setCurrentTransitionStyleTo(key: string){
      this.transitionStyle = key;
    }
}