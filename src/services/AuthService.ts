import firebase from 'firebase';

export class AuthService{

    currentUid: string

    getActiveUser(){
        return firebase.auth().currentUser;
    }
    
    signup(email: string, password: string) {
        return firebase.auth().createUserWithEmailAndPassword(email,password);
    }
    
    signin(email:string, password: string) {
        return firebase.auth().signInWithEmailAndPassword(email, password);
    }
 
    logout(){
        firebase.auth().signOut();
    } 

    setCurrentUser(uid: string){
        this.currentUid = uid
    }

    getCurrentUser(){
        return this.currentUid
    }
}