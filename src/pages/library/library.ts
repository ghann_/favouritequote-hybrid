import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, IonicPage} from 'ionic-angular';
import { Quote } from '../../data/quote.interface';
import  quotes  from '../../data/quote'
import { QuotesPage } from '../quotes/quotes'; 

@IonicPage()
@Component({
  selector: 'page-library',
  templateUrl: 'library.html'
})
export class LibraryPage implements OnInit{
  quotesPage = QuotesPage
  quoteCollection: {category: string, quotes: Quote[], icon: string}[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit(){
    this.quoteCollection = quotes;
    console.log(this.quoteCollection);
  }
}