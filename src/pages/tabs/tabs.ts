import { Component } from '@angular/core';
import { LibraryPage } from '../library/library';
import { FavoritePage } from '../favorite/favorite';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = FavoritePage;
  tab2Root = LibraryPage;

  constructor() {
  }
}
