import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Quote } from '../../data/quote.interface';
import { QuotesService } from '../../services/quotes';
import quotes from '../../data/quote';

@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html',
})
export class QuotesPage {
  quotesPicked: {category: string, quotes: Quote[]};
  quotes: Quote[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private quotesService: QuotesService) {
  }

  ionViewDidLoad(){
    this.quotesPicked = this.navParams.data;
    this.quotes = this.navParams.data.quotes;
  }

  onShowAlert(quote: Quote){
    const alert = this.alertCtrl.create({
      title: 'Add Quote',
      message: 'Are you sure you want to add the quote to favorites?',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          handler: () => {
            this.onAddQuote(quote);
            console.log(quote);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    alert.present();
  }

  onShowAlertUnfavorite(quote: Quote){
    const alert = this.alertCtrl.create({
      title: 'Add Quote',
      message: 'Are you sure you want to unfavorite this quote?',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          handler: () => {
            this.onRemoveQuote(quote);
            console.log(this.quotesService);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    alert.present();
  }

  onAddQuote(quote: Quote){
    this.quotesService.addQuoteToFavorites(quote);
    console.log(this.quotesService);
  }

  onRemoveQuote(quote: Quote){
    this.quotesService.removeQuoteFromFavorites(quote);
    console.log(this.quotesService);
  }
}
