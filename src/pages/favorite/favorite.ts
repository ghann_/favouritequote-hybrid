import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController, ActionSheetController, LoadingController } from 'ionic-angular';
import { QuotesService } from '../../services/quotes';
import { QuotePage } from '../quote/quote';
import { Quote } from '../../data/quote.interface';
import  quotes  from '../../data/quote';
import { SettingService } from '../../services/settings';
import { AuthService } from "../../services/AuthService";
import { PopoverController } from 'ionic-angular'; 
import { PopoverPage } from '../popover/popover';
import firebase from 'firebase';

@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html',
})
export class FavoritePage {
  quotesPicked: {category: string, quotes: Quote[]};
  quotes: Quote[];
  listColor: String = "#C1FFC1"
  uid = this.authService.getCurrentUser()
  fromDB: Quote[];

  constructor(private quotesService: QuotesService, public alertCtrl: AlertController, public toastCtrl: ToastController, private quoteService: QuotesService, 
              private actshtCtrl: ActionSheetController, public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, 
              private settingService: SettingService, public popoverCtrl: PopoverController, public authService: AuthService, private loadingCtrl: LoadingController) {
  }

  ngOnInit(){
    let loading = this.loadingCtrl.create({
      content: 'Processing'
    });
    loading.present()

    const quotesRef: firebase.database.Reference = firebase.database().ref("/" + this.uid + "/fav-quotes");
    quotesRef.on('value', quoteSnapshot => {
      this.fromDB = quoteSnapshot.val()

      this.quoteService.removeAllQuote()
      for(let i=0; i<this.fromDB.length; i++){
        this.quoteService.addQuoteToFavorites(this.fromDB[i])
      }
    
    });

    this.quotes = this.quoteService.getFavoriteQuotes();
    this.settingService.isBackgroundChanged() == true ? this.listColor = "#ffee90" : this.listColor = "#C1FFC1";

    loading.dismiss();
  }


  ionViewWillEnter() {
    this.quotes = this.quoteService.getFavoriteQuotes();
    this.settingService.isBackgroundChanged() == true ? this.listColor = "#ffee90" : this.listColor = "#C1FFC1";
  }

  presentPopover(myEvent) { 
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({       
      ev: myEvent     
    });
    popover.onWillDismiss(() => {
      this.ionViewWillEnter()
    })
  } 

  presentActionSheet(quote: any) {
    const actionSheet = this.actshtCtrl.create({
      title: 'Quote Options',
      buttons: [
        {
          text: 'Delete',
          role: 'destructive',
          handler: () => {
            this.quotesService.removeQuoteFromFavorites(quote);
            this.navCtrl.setRoot(this.navCtrl.getActive().component);
          }
        },
        {
          text: 'Show Quote Detail',
          handler: () => {
            this.favoriteClick(quote);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
 
    actionSheet.present();
  }

  favoriteClick(quote: any){
    let modal = this.modalCtrl.create(QuotePage, quote);
    modal.onWillDismiss(flag => {
     if(flag.removed == 1){
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
     }
    });
    modal.present();
  }
}
