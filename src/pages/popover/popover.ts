import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { QuotesService } from '../../services/quotes';
import { QuotePage } from '../quote/quote';
import { Quote } from '../../data/quote.interface';
import  quotes  from '../../data/quote';
import { FavoritePage } from '../favorite/favorite';
import { AuthService } from "../../services/AuthService";

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
  template: ` <div style="text-align: center; padding:5px">       
    <button ion-button outline (click)="btnSavePopOver()">Save</button>       
    <button ion-button outline (click)="btnAddQuotePopOver()">Add Quote</button>       
    <button ion-button outline (click)="btnClearAllPopOver()">Clear All</button>  
    </div>
` 
})
export class PopoverPage { 
  quotes: Quote[];

  constructor(private quoteService: QuotesService, public viewCtrl: ViewController, private alertCtrl: AlertController, 
              private toastCtrl: ToastController, public authService: AuthService, private loadingCtrl: LoadingController) {}   
  
  ionViewWillEnter(){
    
  }

  btnSavePopOver(){
    let loading = this.loadingCtrl.create({
      content: 'Processing'
    });
    loading.present()

    this.authService.getActiveUser().getToken()
    .then((token:string) => {
      const uid = this.authService.getActiveUser().uid
      this.quoteService.storeList(token, uid).subscribe( () => this.presentToast("Save Success"),
        error => {
          console.log(error);
        });
      });
      loading.dismiss()   
  }

  btnAddQuotePopOver(){
    this.btnAddQuoteClicked()
  }

  btnClearAllPopOver(){
    let loading = this.loadingCtrl.create({
      content: 'Processing'
    });
    loading.present()

    this.quoteService.removeAllQuote()
    this.presentToast("Quotes Successfully Removed")
    //this.btnSavePopOver()

    loading.dismiss();
  }

  btnAddQuoteClicked(){
    const alert = this.alertCtrl.create({
      title: 'Add New Quote',
      inputs: [{
          name: 'AuthorText',
          placeholder: 'Author'
         },
         {
          name: 'QuoteText',
          placeholder: 'Quote'
          }
      ],
      buttons: [{
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Ok',
          handler: data => {
            if ((data.AuthorText.trim() != '' && data.AuthorText != null) && data.QuoteText.trim() != '' && data.QuoteText != null)  {
               this.processAddQuote(data);
            }else{
              this.presentToast("Fields can't be empty.");
            }
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  presentToast(message: string){
    const toast = this.toastCtrl.create({
      message: message,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  processAddQuote(data){
    const newQuote: Quote =  {
        id: null,
        person: data.AuthorText,
        text: data.QuoteText
      }
      this.quoteService.addQuoteToFavorites(newQuote);
      this.presentToast("Quote was added successfully");
      this.quotes = this.quoteService.getFavoriteQuotes();
  }   
} 
