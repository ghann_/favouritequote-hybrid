import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { QuotesService } from '../../services/quotes';
import { Quote } from '../../data/quote.interface';
import  quotes  from '../../data/quote'

@IonicPage()
@Component({
  selector: 'page-quote',
  templateUrl: 'quote.html',
})
export class QuotePage {
  quotesPicked: {category: string, quotes: Quote[]};
  quote: Quote;

  constructor(private quotesService: QuotesService, public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, private viewCtrl: ViewController) {
  }

  ngOnInit() {
    this.quote = this.navParams.data;
  }

  closeModal(){
    let flag = {
      removed : "0"
    }
    this.viewCtrl.dismiss(flag);
  }

  onRemoveQuote(quote: Quote){
    let flag = {
      removed : 1
    }
    this.quotesService.removeQuoteFromFavorites(quote);
    this.viewCtrl.dismiss(flag);
  }
}