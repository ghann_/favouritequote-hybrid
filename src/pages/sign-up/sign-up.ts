import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { AuthService } from '../../services/AuthService';

@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  userForm: FormGroup;
  
    constructor(public navCtrl: NavController, public navParams: NavParams, private authService: AuthService, private alertCtrl: AlertController) {
      this.initializeForm();
    }
  
    private initializeForm() {
      this.userForm =   new FormGroup({
        email:          new FormControl(null, Validators.required),
        password:       new FormControl(null, Validators.required)
      })
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }
  onSubmit() {
    this.authService.signup(this.userForm.value.email, this.userForm.value.password).catch((error)=>{
      this.showAlert(error)
    })
  }
  
  showAlert(error: string){
    const alert = this.alertCtrl.create({
      title: 'Error',
      message: error,
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          handler: () => {

          }
        }
      ]
    });
    alert.present();
  }
}
