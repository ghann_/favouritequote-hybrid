import { Config } from 'ionic-angular';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { SettingService } from "../../services/settings"

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  public selectedTransition:string
  public selectedTabs:string
  public toggleValue: Boolean
  public currenTabsPlacement: string
  public currentTransitionStyle: string
  
  public transStateIos: boolean
  public transStateAnd: boolean
  public tabStateTop: boolean
  public tabStateBottom: boolean

  constructor(private config: Config,private settingServices: SettingService, public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController) {
  }

  handleToggle(e) {
    this.settingServices.changeBackground(e);
    this.presentToast(0);
 }

  ionViewWillEnter(){
    this.toggleValue = this.settingServices.isBackgroundChanged()

    this.tabStateTop    = this.settingServices.getCurrentTabsPlacement() == "top" ? true : false
    this.tabStateBottom = this.settingServices.getCurrentTabsPlacement() == "bottom" ? true : false
    this.selectedTabs   = this.settingServices.getCurrentTabsPlacement() == "top" ? "Top of the page" : "Bottom of the page"
    this.transStateIos  = this.settingServices.getCurrentTransitionStyle() == "ios" ? true : false
    this.transStateAnd  = this.settingServices.getCurrentTransitionStyle() == "and" ? true : false
    this.selectedTransition = this.settingServices.getCurrentTransitionStyle() == "ios" ? "IOS Style" : "Android Style"
  }

  tabsPlacementChanged(to){
    this.currenTabsPlacement = to
    this.settingServices.setCurrentTabsPlacementTo(this.currenTabsPlacement)
  }

  transitionStyleChanged(to){
    this.currentTransitionStyle = to
    this.settingServices.setCurrentTransitionStyleTo(this.currentTransitionStyle)
  }

  presentToast(flag) {
    let info = flag == 0 ? this.settingServices.isBackgroundChanged() == true ? "Semi-yellow" : "Semi-green" : "Config has been applied"
    let toast = this.toastCtrl.create({
      message: flag == 0 ? 'Background changed to ' + info : info,
      duration: 1000,
      position: 'bottom'
    });
  
    toast.present();
  }

  applyChangesClicked(){
      this.currenTabsPlacement == "top" ? this.config.set("tabsPlacement", "top") : this.config.set("tabsPlacement", "bottom")
      this.currentTransitionStyle == "ios" ? this.config.set("pageTransition", "ios") : this.config.set("pageTransition", "android")
      this.presentToast(1)
  }
}
